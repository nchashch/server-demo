FROM ubuntu:19.04
RUN mkdir -p /opt/serverdemo/
ARG BINARY_PATH
WORKDIR /opt/serverdemo
RUN apt-get update && apt-get install -y \
  libpq-dev \
  libc6-dev \
  libmpc-dev
COPY "$BINARY_PATH" /opt/serverdemo/serverdemo
CMD ["/opt/serverdemo/serverdemo"]
