# server-demo
## Зависимости
- stack (https://docs.haskellstack.org/en/stable/README/)
- docker с docker-compose
## Usage
Чтобы запустить сервер нужно выполнить скрипт:
``` bash
./deploy.sh
```
Скрипт построит docker образ для приложения и выполонит docker-compose up.
