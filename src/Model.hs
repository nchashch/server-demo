{-# LANGUAGE DeriveGeneric #-}
module Model where

import GHC.Generics
import Data.Aeson

data Thing = Thing {
  id :: Int,
  foo :: Int,
  bar :: String
  } deriving (Generic, Show)

instance ToJSON Thing
instance FromJSON Thing
