{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main (main) where

import Data.Aeson (encode, decode)
import Network.Socket (Socket)
import qualified Data.ByteString.Char8 as S
import qualified Data.ByteString.Lazy.Char8 as L
import Data.ByteString.Lazy (toStrict)
import Database.PostgreSQL.Simple
import Control.Monad (void, forM, join)
import qualified Data.Text as Text

import Model
import Http
import HttpRequestParser

main :: IO ()
main = do
  let connectionString = "host=database dbname=postgres user=postgres password=postgres"
  conn <- connectPostgreSQL connectionString
  initDB conn
  runTCPServer Nothing "3000" (server conn)

server :: Connection -> Socket -> IO ()
server conn s = do
  eitherRequest <- recvRequest s
  let Right request = eitherRequest
  case reqMethod request of
    Get -> getThings
    Post -> postThing (parseBody request)
  where
    getThings :: IO ()
    getThings = do
      things <- selectThings conn
      send200Response s $ (toStrict . encode) things

    postThing :: Maybe Thing -> IO ()
    postThing maybeThing =
      case maybeThing of
        Just thing -> do
          insertThing conn thing
          send200Response s $ (toStrict . encode) thing
        Nothing -> send500Response s


    parseBody :: HttpRequest -> Maybe Thing
    parseBody req = join $ (decode . L.pack) `fmap` reqBody req


initDB :: Connection -> IO ()
initDB conn = do
  void $ execute_ conn "create table if not exists thing (id serial not null primary key, foo integer not null, bar varchar(256) not null)"

selectThings :: Connection -> IO [Thing]
selectThings conn = do
  xs :: [(Int, Int, Text.Text)] <- query_ conn "select id, foo, bar from thing"
  things <- forM xs $ \(id, foo, bar) ->
    return $ Thing (id :: Int) (foo :: Int) (Text.unpack bar)
  return things

insertThing :: Connection -> Thing -> IO ()
insertThing conn thing = void $ execute conn "insert into thing (foo, bar) values (?, ?)" (foo thing :: Int, bar thing :: String)
